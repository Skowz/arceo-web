<?php

namespace App\Controller;

use App\Entity\Authentification;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PinController extends AbstractController
{

    public function accueil(/*EntityManagerInterface $auth*/) : Response
    {
        /*
        $repo = $auth->getRepository(Authentification::class);

        $pins = ($repo->findAll()); 
        */

        return $this->render('pages/Accueil.html.twig'/*, ['name' --> 'Bertrand']*/);

    }

    public function services() : Response
    {
        return $this->render('pages/Services.html.twig');

    }

    public function formateurs() : Response
    {
        return $this->render('pages/Formateurs.html.twig');

    }

    public function recrutement() : Response
    {
        return $this->render('pages/Recrutement.html.twig');

    }

    public function apropos() : Response
    {
        return $this->render('pages/Apropos.html.twig');

    }

    public function contact() : Response
    {
        return $this->render('pages/Contact.html.twig');

    }

    public function connexion(Request $request)
    {
        if ($request->isMethod('POST')) 
        {
            dd($request->request);
        }
        
        return $this->render('pages/Connexion.html.twig');

    }
}
